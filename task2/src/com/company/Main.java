package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.io.*;


public class Main {

    public static void main(String[] args) {

        try (FileWriter writer = new FileWriter("C:/temp/task2.txt", false)) {
            String text = "3,8,1,18,6,16,13,17,20,4,5,14,11,0,7,9,15,10,12,19";
            writer.write(text);
            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
        System.out.println("Числа от 0 до 20 записаны в файл C:/temp/task2.txt");
    }
}
class SortNumbers {
    public static void main(String[] args) throws IOException {

        System.out.println("Открываем файл...");
        FileReader fin = new FileReader("C:/temp/task2.txt");
        int c;
        int number=0;
        boolean ncheck = false;
        ArrayList<Integer> n = new ArrayList<Integer>();
        System.out.println("Считываем данные....");
       while ((c = fin.read()) != -1){
            if (c>=48 && c<58){
                number = number*10+Character.getNumericValue((char)c);
                ncheck=true;
            }
            else if(ncheck==true){
                n.add(number);
                number=0;
                ncheck=false;
            }
        }
        if(ncheck==true){
            n.add(number);
            number=0;
            ncheck=false;
        }

        System.out.println("Сортировка данных по возрастанию");
        Collections.sort(n);
        System.out.println(n);


        System.out.println("Сортировка данных по убыванию");
        Collections.reverse(n);
        System.out.println(n);
    }

}