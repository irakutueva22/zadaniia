package com.company;


public class Main {
    public static void main(String[] args) {
        long n = 20;
        do {
            long result = 1;
            for (int i = 2; i <= n; i++)
                result *= i;
            System.out.println(result);
        }
        while (n < 0);
    }
}